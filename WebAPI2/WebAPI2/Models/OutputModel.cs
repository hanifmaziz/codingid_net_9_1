﻿namespace WebAPI2.Models
{
	public class OutputModel
	{
		public int UserId { get; set; }
		public string? Name { get; set; }
		public List<CourseModel>? Courses { get; set; }
	}
}
