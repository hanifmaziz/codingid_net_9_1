﻿namespace WebAPI2.Models
{
	public class AccountBalanceModel
	{
		public int Pk_Account_Balance_Id { get; set; }
		public string? Account_Name { get; set; }
		public decimal Balance { get; set; }
	}
}
