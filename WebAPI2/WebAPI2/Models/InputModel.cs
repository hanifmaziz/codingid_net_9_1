﻿namespace WebAPI2.Models
{
	public class InputModel
	{
		public string? name { get; set; }
		public List<CourseModel>? courses { get; set; }
	}
}
