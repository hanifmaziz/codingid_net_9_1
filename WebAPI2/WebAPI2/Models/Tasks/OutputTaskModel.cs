﻿namespace WebAPI2.Models.Tasks
{
	public class OutputTaskModel
	{
		public List<TasksModel>? tasks { get; set; }
		public int pk_users_id { get; set; }
		public string? name { get; set; }
	}
}
