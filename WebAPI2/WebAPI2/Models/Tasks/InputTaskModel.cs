﻿namespace WebAPI2.Models.Tasks
{
	public class InputTaskModel
	{
		public string? name { get; set; }
		public List<TasksModel>? tasks { get; set; }
	}
}
