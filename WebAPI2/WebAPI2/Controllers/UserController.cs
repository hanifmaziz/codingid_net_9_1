﻿using Google.Protobuf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Data;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		public UserController(IConfiguration configuration)
		{
			_configuration = configuration;
		}
		[HttpGet]
		[Authorize]
		[Route("GetUser")]
		public IActionResult GetItem(string? userName)
		{
			List<UserModel> output = new List<UserModel>();
			using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				string query = "";
				MySqlCommand cmd = new MySqlCommand(query, conn);

				if (string.IsNullOrEmpty(userName))
				{
					query = "SELECT * From tbl_User a INNER JOIN tbl_Course b on a.UserId = b.fk_user_id";
					cmd = new MySqlCommand(query, conn);
				}
				else
				{
					//query = "SELECT * From tbl_User where UserName ='" + userName + "'";
					query = "SELECT * From tbl_User where UserName ='@username'";
					cmd = new MySqlCommand(query, conn);
					cmd.Parameters.AddWithValue("@username", userName);
				}

				MySqlDataAdapter dtAdapter = new MySqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				dtAdapter.Fill(dt);
				bool validateIsActive = false;

				foreach (DataRow dr in dt.Rows)
				{
					UserModel user = new UserModel();
					user.UserId = Convert.ToInt32(dr["UserId"].ToString());
					user.FirstName = dr["FirstName"].ToString();
					user.LastName = dr["LastName"] == (object)DBNull.Value ? "" : dr["LastName"].ToString();
					user.UserName = dr["UserName"] == (object)DBNull.Value ? "" : dr["UserName"].ToString();
					user.Email = dr["Email"] == (object)DBNull.Value ? "" : dr["Email"].ToString();
					user.UserRole = dr["UserRole"] == (object)DBNull.Value ? "" : dr["UserRole"].ToString();
					user.IsActive = dr["IsActive"] == (object)DBNull.Value ? false : Convert.ToBoolean(dr["IsActive"]);
					validateIsActive = dr["IsActive"] == (object)DBNull.Value ? false : Convert.ToBoolean(dr["IsActive"]);

					if (validateIsActive)
					{
						output.Add(user);
					}
				}


				//using (MySqlDataReader reader = cmd.ExecuteReader())
				//{
				//	while (reader.Read())
				//	{
				//		output.Add(new UserModel()
				//		{
				//			UserId = reader.GetInt32("UserId"),
				//			FirstName = reader.GetString("FirstName"),
				//			LastName = reader.GetString("LastName"),
				//			UserName = reader.GetString("UserName"),
				//			Email = reader.GetString("Email"),
				//			Password = reader.GetString("Password"),
				//			UserRole = reader.GetString("UserRole"),
				//			IsActive = reader.GetBoolean("IsActive")
				//		});
				//	}
				//}

				conn.Close();
			}
			return Ok(output);
		}

		[HttpPost]
		[Route("AddUser")]
		public IActionResult AddItem([FromBody] UserModel? user)
		{
			using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				string query = "Insert Into tbl_User (`FirstName`, `LastName`, `UserName`, `Email`, `Password`, `UserRole`, `IsActive`) Values('" + user.FirstName + "', '" + user.LastName + "', '" + user.UserName + "', '" + user.Email + "', '" + user.Password + "', '" + user.UserRole + "', " + false + ")";
				MySqlCommand cmd = new MySqlCommand(query, conn);
				cmd.ExecuteNonQuery();

				conn.Close();
			}
			return Ok("Item Added Succesfully");
		}

		[HttpPut("UpdateUser")]
		public IActionResult UpdateItem(string userName)
		{
			using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				string query = "Update tbl_User set isActive =1 Where UserName ='" + userName + "'";
				MySqlCommand cmd = new MySqlCommand(query, conn);
				cmd.ExecuteNonQuery();
				conn.Close();
			}
			return Ok("Item Update Successfully");
		}

	}
}
