﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Data;
using System.Runtime.CompilerServices;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserCourseController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		public UserCourseController(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		[HttpPost]
		[Route("AddUserCourse")]
		public IActionResult AddUserCourse([FromBody] InputModel? input)
		{
			using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				//bool validate = GetValidateUser(input.name);
				//if (validate)
				//{
				conn.Open();
				string queryInsetUser = "Insert Into tbl_User (`FirstName`) Values('" + input.name + "'); SELECT LAST_INSERT_ID();";
				MySqlCommand cmd = new MySqlCommand(queryInsetUser, conn);

				var insertedID = cmd.ExecuteScalar();
				foreach (CourseModel cModel in input.courses)
				{
					string queryInsertCourse = "Insert Into tbl_Course (`course_detail`, `fk_user_id`) Values ('" + cModel.Course_Detail + "', " + insertedID + ")";
					cmd = new MySqlCommand(queryInsertCourse, conn);
					cmd.ExecuteNonQuery();
				}
				conn.Close();
				return Ok("Item Added");

				//}
				//else
				//{
				//	return NotFound("User Not Found");
				//}

			}
		}

		[HttpGet]
		[Route("GetUserCourse")]
		public IActionResult GetUserCourse()
		{
			try
			{
				List<OutputModel> output = new List<OutputModel>();
				using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
				{
					conn.Open();
					string queryUser = "SELECT * from tbl_User";
					MySqlCommand cmdUser = new MySqlCommand(queryUser, conn);
					MySqlDataAdapter dtAdapterUser = new MySqlDataAdapter(cmdUser);
					DataTable dtUser = new DataTable();
					dtAdapterUser.Fill(dtUser);

					string queryCourse = "SELECT * from tbl_Course";
					MySqlCommand cmdCourse = new MySqlCommand(queryCourse, conn);
					MySqlDataAdapter dtAdapterCourse = new MySqlDataAdapter(cmdCourse);
					DataTable dtCourse = new DataTable();
					dtAdapterCourse.Fill(dtCourse);

					foreach(DataRow drUser in dtUser.Rows)
					{
						OutputModel outModel = new OutputModel();
						outModel.UserId = Convert.ToInt32(drUser["UserId"].ToString());
						outModel.Name = drUser["UserName"].ToString();
						List<CourseModel> listCourseModel = new List<CourseModel>();
						foreach(DataRow drCourse in dtCourse.Rows)
						{
							if (drUser["UserId"].ToString() == drCourse["fk_user_id"].ToString())
							{
								CourseModel cModel = new CourseModel();
								cModel.Pk_Course_Id = Convert.ToInt32(drCourse["course_id"].ToString());
								cModel.Course_Detail = drCourse["course_detail"].ToString();
								listCourseModel.Add(cModel);
							}
						}
						outModel.Courses = listCourseModel;
						output.Add(outModel);
					}



					conn.Close();
				}
				return Ok(output);
			}
			catch (Exception ex)
			{
				return Ok(ex.ToString());
			}
		}


		//private bool GetValidateUser(string name)
		//{
		//	if(name == "hanif")
		//	{
		//		return true;
		//	}
		//	else
		//	{
		//		return false;
		//	}
		//}

	}
}
