﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Data;
using WebAPI2.Models;
using WebAPI2.Models.Tasks;

namespace WebAPI2.Controllers.TasksControler
{
	[Route("api/tasks")]
	[ApiController]
	public class tasksController : ControllerBase
	{
		private readonly IConfiguration _configuration;
		public tasksController(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		//[HttpPost]
		//[Route("AddUserWithTask")]
		//public IActionResult AddUserWithTask([FromBody] InputTaskModel? inputUserTask)
		//{
		//	using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
		//	{
		//		conn.Open();
		//		string queryUser = "Insert into Users (`name`) Values ('@name'); Select LAST_INSERT_ID() ";
		//		MySqlCommand cmd = new MySqlCommand(queryUser, conn);
		//		cmd.Parameters.AddWithValue("@name", inputUserTask.name);

		//		var insertedID = cmd.ExecuteScalar();

		//		string queryTask = "Insert into Tasks (`taks_detail`, `fk_users_id`) Values ('@task_detail', @fk_users_id)";
		//		foreach (TasksModel tasks in inputUserTask.tasks)
		//		{
		//			cmd = new MySqlCommand(queryTask, conn);
		//			cmd.Parameters.AddWithValue("@task_detail", tasks);
		//			cmd.Parameters.AddWithValue("@fk_users_id", insertedID);
		//			cmd.ExecuteNonQuery();
		//		}

		//		conn.Close();
		//	}
		//	return Ok("Item Added Successfully");
		//}

		[HttpGet]
		[Route("GetUserWithTask")]
		public IActionResult GetUserWithTask(string? name)
		{
			List<OutputTaskModel> output = new List<OutputTaskModel>();

			using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				string queryUser = "";
				MySqlCommand cmdUser = new MySqlCommand(queryUser, conn);

				if (string.IsNullOrEmpty(name))
				{
					queryUser = "Select * from users";
					cmdUser = new MySqlCommand(queryUser, conn);
				}
				else
				{
					queryUser = "Select * from users where name=@name";
					cmdUser = new MySqlCommand(queryUser, conn);
					cmdUser.Parameters.AddWithValue("@name", name);
				}

				MySqlDataAdapter dtAdapterUser = new MySqlDataAdapter(cmdUser);
				DataTable dtUser = new DataTable();
				dtAdapterUser.Fill(dtUser);

				string queryTasks = "Select * from tasks";
				MySqlCommand cmdTasks = new MySqlCommand(queryTasks, conn);
				MySqlDataAdapter dtAdapterTasks = new MySqlDataAdapter(cmdTasks);
				DataTable dtTasks = new DataTable();
				dtAdapterTasks.Fill(dtTasks);


				foreach (DataRow drUser in dtUser.Rows)
				{
					OutputTaskModel outModel = new OutputTaskModel();
					outModel.pk_users_id = Convert.ToInt32(drUser["pk_users_id"].ToString());
					outModel.name = drUser["name"].ToString();
					List<TasksModel> listTaskModel = new List<TasksModel>();
					foreach (DataRow drTasks in dtTasks.Rows)
					{
						if (drUser["pk_users_id"].ToString() == drTasks["fk_users_id"].ToString())
						{
							TasksModel cModel = new TasksModel();
							cModel.pk_tasks_id = Convert.ToInt32(drTasks["pk_tasks_id"].ToString());
							cModel.task_detail = drTasks["taks_detail"].ToString();
							listTaskModel.Add(cModel);
						}
					}
					outModel.tasks = listTaskModel;
					output.Add(outModel);
				}

				conn.Close();
			}


			return Ok(output);
		}
	}
}
