﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using WebAPI2.Models;

namespace WebAPI2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AccountBalanceController : ControllerBase
	{
		#region Configuration
		private readonly IConfiguration _configuration;
		public AccountBalanceController(IConfiguration configuration)
		{
			_configuration = configuration;
		}
		#endregion


		#region Routes
		[HttpGet]
		[Route("GetAccountBalance")]
		public IActionResult GetAccountBalance()
		{
			List<AccountBalanceModel> output = new List<AccountBalanceModel>();
			string query1 = "Update tbl_account_balance set balance = balance -100 where account_name = 'Hanif'";
			string query2 = "Update tbl_account_balance1 set balance = balance +100 where account_name = 'Maziz'";
			string queryGetItem = "Select * from tbl_account";

			using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
			{
				conn.Open();
				MySqlTransaction transaction = conn.BeginTransaction();

				try
				{
					MySqlCommand cmd = new MySqlCommand(query1, conn);
					cmd.ExecuteNonQuery();
					transaction.Commit();

					cmd = new MySqlCommand(query2, conn);
					cmd.ExecuteNonQuery();
				}
				catch(Exception ex)
				{
					transaction.Rollback();
				}


				conn.Close();
			}

			return Ok(output);
		}
		#endregion 
	}
}
